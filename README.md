# Nox Weather Charts

This software suite let's you create charts based on NOXnet weather data.

**Installation**

Preperation steps (asuming there is no python virtualenv installed yet)
1.  Install python3 pip `sudo apt-get install python3-pip`
2.  Install virtualenv using pip `sudo pip3 install virtualenv`
3.  Setup python virtual environment `virtualenv <yourvenvname>` 
4.  Activate python virtual environment `source <yourvenvname>/bin/activate`

Preparation steps (asuming python virtualenv is allready running)
1.  Generate pip requirements file `touch pip_requirements.txt`
2.  Populate your requirements file with the list down below `vim pip_requirements.txt`
3.  Install/update your python libraries `pip install -r pip_requirements.txt`
4.  Clone or download this project into your home directory

pip_requirements.txt
```
2to3==1.0
certifi==2019.6.16
chardet==3.0.4
chart-studio==1.0.0
idna==2.8
plotly==4.1.0
requests==2.22.0
retrying==1.3.3
six==1.12.0
urllib3==1.25.3
```

**Plotly Account**

Sign in/up to [plotly chart studio](https://chart-studio.plot.ly/) and read about using their API to publish the data. In order to make it run you have to localy configure a user and an api-key as a minimum requirement.

**Task Scheduler**

Best practise is to run the task from 7am to 9pm every 2 hours (8 executions per day).
*  Synology Disk Station 
```
su - admin && export USER=admin && export HOME="/var/services/homes/admin" && cd /var/services/homes/admin/ && source <yourpythonvenv>/bin/activate && cd <pathtoyourgitlabrepo>/ && wget --http-user=administrator --http-password=<noxnetpassword> http://<noxnetmasteripaddress>:5001/maintenance/logs/weather/noxnet_weather_000.log?action=download -O noxnet_weather_000.log && wget --http-user=administrator --http-password=<noxnetpassword> http://<noxnetmasteripaddress>:5001/maintenance/logs/weather/noxnet_weather_001.log?action=download -O noxnet_weather_001.log && /var/services/homes/admin/noxnet/noxweathercharts.py && /var/services/homes/admin/noxnet/noxweathercharts_24h.py && /var/services/homes/admin/noxnet/noxweathercharts_7d.py
```
*  Cronjob
```
3 7,9,11,13,15,17,19,21 * * * source <yourpythonvenv>/bin/activate && cd <pathtoyourgitlabrepo>/ && wget --http-user=administrator --http-password=<noxnetpassword> http://<noxnetmasteripaddress>:5001/maintenance/logs/weather/noxnet_weather_000.log?action=download -O noxnet_weather_000.log && wget --http-user=administrator --http-password=<noxnetpassword> http://<noxnetmasteripaddress>:5001/maintenance/logs/weather/noxnet_weather_001.log?action=download -O noxnet_weather_001.log && /var/services/homes/admin/noxnet/noxweathercharts.py && /var/services/homes/admin/noxnet/noxweathercharts_24h.py && /var/services/homes/admin/noxnet/noxweathercharts_7d.py >/dev/null 2>&1
```
#!/bin/env python3

import csv
import chart_studio.plotly as py
import plotly.graph_objs as go

noxnetfile = open('noxnet_weather_000.log')
noxnetreader = csv.reader(noxnetfile,delimiter='\t')
noxnetdata0 = list(noxnetreader)

noxnetfile = open('noxnet_weather_001.log')
noxnetreader = csv.reader(noxnetfile,delimiter='\t')
noxnetdata1 = list(noxnetreader)

noxnetdata = noxnetdata1 + noxnetdata0

xdata = []
ydata_east = []
ydata_south = []
ydata_west = []

for row in noxnetdata[-145:]:
    xdata.append(row[0][:-4] + ' ' + row[1][:-3])
    ydata_east.append(row[21])
    ydata_south.append(row[24])
    ydata_west.append(row[27])

lux_east = go.Scatter(name='lux_ost', x = xdata, y = ydata_east)
lux_south = go.Scatter(name='lux_sued', x = xdata, y = ydata_south)
lux_west = go.Scatter(name='lux_west', x = xdata, y = ydata_west)

data = [lux_east, lux_south, lux_west]

py.plot(data, filename = 'lux_nox_24h', auto_open=True)